<?php
#Name:Remove Lines From String v1
#Description:Remove specific lines from a string. Returns the modified string on success. Returns "FALSE" on failure.
#Notes:Optional arguments can be "NULL" to skip them in which case they will use default values. Line ending type is not maintained.
#Arguments:'string' (required) is a string containing the data to remove lines from. 'line_numbers' (required) is an array of numbers indicating which lines to remove. 'display_errors' (optional) indicates if errors should be displayed.
#Arguments (Script Friendly):string:string:required,line_numbers:array:required,display_errors:bool:optional
#Content:
if (function_exists('remove_lines_from_string_v1') === FALSE){
function remove_lines_from_string_v1($string, $line_numbers, $display_errors = NULL){
	$errors = array();
	$progress = '';
	##Arguments
	if ($string === ''){
		$errors[] = 'string';
	}
	if (@is_array($line_numbers) === FALSE){
		$errors[] = 'line_numbers';
	} else {
		$check = @implode($line_numbers);
		if (@ctype_digit($check) === FALSE){
			$errors[] = 'line_numbers';
		}
		unset($check);
	}
	if ($display_errors === NULL){
		$display_errors = FALSE;
	} else if ($display_errors === TRUE){
		#Do nothing
	} else if ($display_errors === FALSE){
		#Do Nothing
	} else {
		$errors[] = "display_errors";
	}
	##Task []
	if (@empty($errors) === TRUE){
		$string = @explode(PHP_EOL, $string);
		foreach ($line_numbers as &$line_number) {
			$string_key = $line_number - 1;
			unset($string[$string_key]);
		}
		$string = @implode(PHP_EOL, $string);
	}
	result:
	##Display Errors
	if ($display_errors === TRUE){
		if (@empty($errors) === FALSE){
			$message = @implode(", ", $errors);
			if (function_exists('remove_lines_from_string_v1_format_error') === FALSE){
				function remove_lines_from_string_v1_format_error($errno, $errstr){
					echo $errstr;
				}
			}
			set_error_handler("remove_lines_from_string_v1_format_error");
			trigger_error($message, E_USER_ERROR);
		}
	}
	##Return
	if (@empty($errors) === TRUE){
		return $string;
	} else {
		return FALSE;
	}
}
}
?>